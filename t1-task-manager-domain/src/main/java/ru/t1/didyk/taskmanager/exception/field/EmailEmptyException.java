package ru.t1.didyk.taskmanager.exception.field;

public final class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException() {
        super("Error! Email is empty.");
    }

}
