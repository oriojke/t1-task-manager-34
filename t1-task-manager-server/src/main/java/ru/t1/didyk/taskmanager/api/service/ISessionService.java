package ru.t1.didyk.taskmanager.api.service;

import ru.t1.didyk.taskmanager.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
