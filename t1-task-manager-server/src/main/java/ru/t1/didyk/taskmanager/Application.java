package ru.t1.didyk.taskmanager;

import ru.t1.didyk.taskmanager.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
