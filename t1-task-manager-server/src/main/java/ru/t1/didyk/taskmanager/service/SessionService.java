package ru.t1.didyk.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.repository.ISessionRepository;
import ru.t1.didyk.taskmanager.api.service.ISessionService;
import ru.t1.didyk.taskmanager.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }
}
